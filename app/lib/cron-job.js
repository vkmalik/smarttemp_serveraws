'use strict';
const express = require('express');
const router = express.Router();
const cron = require('node-cron');
const request = require('request-promise-native');
const uuid = require('uuid-pure').newId;
const { Schedule, DeviceParam, OverRide } = require('../models');
const moment = require('moment-timezone');
const controlParameters = [
    'equip_mode',
    'fan_speed',
    'temp_cool',
    'temp_heat',
    'occupied',
    'relay_status',
    'wifi_lost',
];



(async function() {
    const cronResult = await Schedule.find({});
    const cronOverRide = await OverRide.find({});
    cronResult.forEach(ele => {
        scheduleUpdate(ele.scheduled, ele.uid)
    })

    cronOverRide.forEach(ele1 => {
        if (ele1.scheduled !== '') {
            scheduleUpdateOverRide(ele1.scheduled, ele1.uid)
        }
    })
}())


async function scheduleOverRidedate(d) {
    const sec = d.getSeconds();
    const min = d.getMinutes();
    const hur = d.getHours();
    const date = d.getDate();
    // const mon = d.getMonth() + 1;
    const mon = d.getMonth() === 12 ? 1 : d.getMonth() + 1;
    return `${sec} ${min} ${hur} ${date} ${mon} *`;
}

async function schedule(data, uid, state) {
    const cronResult = await Schedule.findOne({ uid });
    if (!cronResult) {
        return;
    }
    if (cronResult.days !== '') {
        await Schedule.findOneAndUpdate({ scheduled: data, uid, enable_disable: true }, { status: 'pending' }, { new: true });
    }

    var scheduleDays = data.split(' ')[5].split(",").map(Number);

    const {
        fan_speed,
        temp_cool,
        temp_heat,
        mac,
        userId,
        overRide,
        scheduledDays,
        days,
        history
    } = cronResult;

    const currentDate = new Date();
    const Day = currentDate.getUTCDay();
    const getPOS = scheduleDays.indexOf(Day);
    if (getPOS === -1) {
        var currentDay = '';
    } else {
        var currentDay = days[getPOS];
    }

    await Schedule.updateMany({ mac }, { scheduleRunning: false })
        // var overRideRunning = false;
        // if (overRide) {
        //     overRideRunning = true
        // }

    let scheduleObj = { 'time': new Date(), 'status': 'schedule true' }
    history = []
    history.push(scheduleObj)

    const result1 = await Schedule.findOneAndUpdate({ scheduled: data, uid }, { scheduleRunning: true, currentDay, history }, { new: true });
    let a = [{ 'date': new Date(), 'from': 'cronJob' }]


    const dataObj = {
        fan_speed,
        temp_cool,
        temp_heat,
        mac,
        userId,
        updatedFrom: a
            // OverRideRunning
    }

    if (state !== false) {
        const deviceResult = await DeviceParam.findOneAndUpdate({ mac }, dataObj);
        const getDayTime = new Date().getTime();
        const getNewValue = Object.assign({}, deviceResult, dataObj)
        const MsgID = uuid(15);
        const finalResult = {
            mac,
            data: {
                MsgID
            }
        };
        controlParameters.forEach(key => {
            finalResult.data[key] = getNewValue[key];
        });

        await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
    }
}


async function overRideScheduleUpdate(uid) {
    const result = await Schedule.find({ uid });
    if (result.length > 0) {
        if (result[0].days.length === 0) {
            await Schedule.findOneAndRemove({ uid }) // to delete previous added schedule as days are empty
        } else {
            await Schedule.findOneAndUpdate({ uid }, { overRideRunning: false, overRideTime: '' }); // to update previous added schedule
        }
    }
}

async function scheduleUpdate(data, uid) {
    cron.schedule(data, async() => {
        const cronResult = await Schedule.findOne({ scheduled: data, uid });
        if (!cronResult) {
            return;
        } else {
            let mac = cronResult.mac;
            let uid = cronResult.uid;
            const over = await OverRide.findOne({ mac });
            if (!over) {
                let state = true;
                schedule(data, uid, state);
            } else {
                // over.forEach(async(ele) => {
                // if (over.mac === mac) {
                if (over.time !== '') {
                    let state = false;
                    //                    await Schedule.findOneAndUpdate({ uid: over.uid }, { overRideRunning: false, overRideTime: '' }); // to update previous added schedule
                    overRideScheduleUpdate(over.uid)
                    await Schedule.findOneAndUpdate({ uid }, { overRideRunning: true, overRideTime: over.time }); // to update current schedule
                    await OverRide.findOneAndUpdate({ mac }, { uid: uid }); // to update current overRide with current running schedule uid
                    schedule(data, uid, state);
                } else {
                    let state = true;
                    //                    await Schedule.findOneAndUpdate({ uid: over.uid }, { overRideRunning: false, overRideTime: '' }); // to update previous added schedule
                    overRideScheduleUpdate(over.uid)
                    await Schedule.findOneAndUpdate({ uid }, { overRideRunning: false, overRideTime: '' }); // to update current schedule
                    await OverRide.findOneAndRemove({ mac }); // to delete current overRide
                    schedule(data, uid, state);
                }
                // }
                // })
            }

        }
        // return;

    });
}


async function scheduleUpdateOverRide(data, uid) {
    cron.schedule(data, async() => {
        const ride = await OverRide.findOne({ scheduled: data });
        let currentDay = ride.currentDay;
        let scheduleId = ride.uid;
        let mac = ride.mac;
        await Schedule.updateMany({ mac }, { scheduleRunning: false }); // to update all the running schedule to false
        //const result = await Schedule.findOneAndUpdate({ uid: scheduleId }, { overRideRunning: false, overRideTime: '', scheduleRunning: true, currentDay: currentDay }, {new:true}); // to update previous added schedule
        const result = await Schedule.findOneAndUpdate({ uid: scheduleId }, { overRideRunning: false, overRideTime: '', scheduleRunning: true }, { new: true }); // to update previous Schedule
        let a = [{ 'date': new Date(), 'from': 'cronJob' }]

        const dataObj = {
            fan_speed: result.fan_speed,
            temp_cool: result.temp_cool,
            temp_heat: result.temp_heat,
            mac: result.mac,
            userId: result.userId,
            updatedFrom: a
        }

        const MsgID = uuid(15);
        const finalResult = {
            mac,
            data: {
                fan_speed: result.fan_speed,
                temp_cool: result.temp_cool,
                temp_heat: result.temp_heat,
                MsgID
            }
        };
        const deviceResult = await DeviceParam.findOneAndUpdate({ mac: result.mac }, dataObj);
        await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
        overRideScheduleUpdate(uid)
        await OverRide.findOneAndRemove({ mac: result.mac });
    });
}

module.exports = {
    schedule: scheduleUpdate,
    overRide: scheduleUpdateOverRide
};