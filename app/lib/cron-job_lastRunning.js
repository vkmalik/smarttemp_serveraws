'use strict';
const cron = require('node-cron');
const request = require('request-promise-native');
const uuid = require('uuid-pure').newId;
const { Schedule, DeviceParam } = require('../models');
const moment = require('moment-timezone');
const controlParameters = [
    'equip_mode',
    'fan_speed',
    'temp_cool',
    'temp_heat',
    'occupied',
    'relay_status',
    'wifi_lost',
];



(async function() {
    const cronResult = await Schedule.find({});
console.log('CRON RUNNING');
    cronResult.forEach(ele => {
console.log(ele.scheduled,'ele');
        scheduleUpdate(ele.scheduled, ele.uid)

    })

}())


function scheduleOverRidedate(d) {
    const sec = d.getSeconds();
    const min = d.getMinutes();
    const hur = d.getHours();
    const date = d.getDate();
    // const mon = d.getMonth() + 1;
    const mon = d.getMonth() === 12 ? 1 : d.getMonth() + 1;
    return `${sec} ${min} ${hur} ${date} ${mon} *`;
}


async function scheduleUpdate(data, uid) {
console.log(data, uid, 'anything new');
    cron.schedule(data, async() => {
	console.log(data,"anything");
        const cronResult = await Schedule.findOneAndUpdate({ scheduled: data, uid, enable_disable: true }, { status: 'completed', scheduleRunning: true }, { new: true });
        if (cronResult.days !== '') {
            await Schedule.findOneAndUpdate({ scheduled: data, uid, enable_disable: true }, { status: 'pending' }, { new: true });
        }
        if (!cronResult) {
            return;
        }
        var scheduleDays = data.split(' ')[5].split(",").map(Number);

        const {
            fan_speed,
            temp_cool,
            temp_heat,
            mac,
            userId,
            overRide,
            scheduledDays,
            days
        } = cronResult;

        console.log(days, 'days')
        const currentDate = new Date();
        console.log('line 62 currentDate.... ', currentDate)
        const Day = currentDate.getUTCDay();
        console.log('line 64 Day.... ', Day)
        const getPOS = scheduleDays.indexOf(Day);
        console.log('line 66 getPOS.... ', getPOS)
        if (getPOS === -1) {
            var currentDay = '';
        } else {
            var currentDay = days[getPOS];
        }

        console.log('line 68 currentDay.... ', currentDay)
        await Schedule.updateMany({ mac, scheduleRunning: true }, { scheduleRunning: false })
        var OverRideRunning = false;
        if (overRide) {
            OverRideRunning = true
        }
        const result1 = await Schedule.findOneAndUpdate({ scheduled: data, uid }, { scheduleRunning: true, OverRideRunning, currentDay }, { new: true });
        const dataObj = {
            fan_speed,
            temp_cool,
            temp_heat,
            mac,
            userId,
            OverRideRunning
        }
        const deviceResult = await DeviceParam.findOneAndUpdate({ mac }, dataObj);
        const getDayTime = new Date().getTime();
        const getNewValue = Object.assign({}, deviceResult, dataObj)
        const MsgID = uuid(15);
        const finalResult = {
            mac,
            data: {
                MsgID
            }
        };
        controlParameters.forEach(key => {
            finalResult.data[key] = getNewValue[key];
        });
        await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
        if (overRide) {
            const { overRideTime } = cronResult || { overRideTime: 1800000 };
            const convNum = Number(overRideTime)
            const getFulldate = (convNum * 1000) + getDayTime;
            const getNewDate = new Date(getFulldate);
            const getdate = scheduleOverRidedate(getNewDate);
            const uid = uuid(15);
            scheduleUpdateOverRide(getdate, uid);
            const { scheduled, enable_disable, fan_type, temp_cool, temp_heat, mac } = cronResult;
            const getData = { fan_type, temp_cool, temp_heat, enable_disable: false, scheduled: getdate, mac, uid };
            const schedule = new Schedule(getData);
            await schedule.save();
            return;
        }
        return;

    });
}


function scheduleUpdateOverRide(data, uid) {
    cron.schedule(data, async() => {
        const cronResult = await Schedule.findOneAndUpdate({ scheduled: data, uid }, { status: 'completed', scheduleRunning: false, OverRideRunning: false }, { new: true });
        if (!cronResult) {
            return;
        }
        const {
            fan_speed,
            temp_cool,
            temp_heat,
            mac,
            userId
        } = cronResult;
        await Schedule.updateMany({ mac }, { scheduleRunning: false, OverRideRunning: false })
        const dataObj = {
            fan_speed,
            temp_cool,
            temp_heat,
            mac,
            userId,
        }
        const deviceResult = await DeviceParam.findOneAndUpdate({ mac }, dataObj, { new: true });
        const MsgID = uuid(15);
        const finalResult = {
            mac,
            data: {
                MsgID
            }
        };
        controlParameters.forEach(key => {
            finalResult.data[key] = deviceResult[key];
        });
        await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
    });
}

module.exports = scheduleUpdate;
