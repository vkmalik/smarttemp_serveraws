const db = require('./db');
const User = require('./user');
const UserProfiles = require('./profiles');
const Blog = require('./blog');
const Contact = require('./contact.js');
const DeviceParam = require('./device-param');
const DeviceParamHistory = require('./history-table');
const Schedule = require('./schedule');
const DeviceRules = require('./device-rules');

module.exports = {
	db,
	User,
	UserProfiles,
	Blog,
	Contact,
	DeviceParam,
    DeviceParamHistory,
	Schedule,
	DeviceRules
};
