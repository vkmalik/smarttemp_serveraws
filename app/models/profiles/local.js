'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
  timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const LocalProfileSchema = new Schema({
  id: { type: String, default: uuid },
  mac: { type: Array },
  userId: { type: String, required: getRequiredFiledMessage('User ID') },
  firstName: { type: String, required: getRequiredFiledMessage('First Name') },
  surName: { type: String, required: getRequiredFiledMessage('Sur Name') },
  password: { type: String, required: getRequiredFiledMessage('Password') },
  isEmailVerified: { type: Boolean, default: false },
  provider: { type: String, default: 'local' },
  profilePic: {type: String, default: ''},
  mobile: {type: String, default: ''},
  city: {type: String, default: ''},
  country: {type: String, default: ''},
  noOfDevice: {type: String, default: ''},
  otp: String
}, options);

const LocalProfile = mongoose.model('tbl_smt400_userprofiles', LocalProfileSchema);
module.exports = LocalProfile;
