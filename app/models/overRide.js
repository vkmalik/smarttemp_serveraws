'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };


const requiredFieldMessage = (filed) => {
    let message = `${filed} Should Not Be Empty`;
    return [true, message]
}


const OverRideSchema = new Schema({
    mac: { type: String, required: requiredFieldMessage('Mac') },
    scheduled: { type: String, default: '' },
    uid: { type: String, default: '' },
    currentDay: { type: String, default: '' },
    time: { type: String, default: '' },
    uid: { type: String, default: '' },
    overRideUTCTime: { type: String, default: '' },
}, options);

module.exports = mongoose.model('tbl_smt400_overrides', OverRideSchema);
