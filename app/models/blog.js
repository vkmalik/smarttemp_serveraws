'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
  timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const BlogSchema = new Schema({
  id: { type: String, default: uuid },
  title: { type: String, required: getRequiredFiledMessage('Blog Title'), trim: true },
  url: { type: String, required: getRequiredFiledMessage('Blog URL'), trim: true },
  imageLink: String,
  description: String,
  postedOn: { type: Date, default: Date.now }
}, options);


const Blog = mongoose.model('tbl_smt400_blog', BlogSchema);
module.exports = Blog;
