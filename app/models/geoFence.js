'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true, versionKey: false };


const GeoSchema = new Schema({

    latitude: { type: String, default: '0' },
    longitude: { type: String, default: '0' },
    radius: { type: String, default: '' },
    event_type: { type: String, enum: [0, 1] },
    temp_heat: { type: String, default: '' },
    temp_cool: { type: String, default: '' },
    fan_type: { type: String, enum: [0, 1] },
    mac: { type: String, default: '' },
    geoFenceName: { type: String, default: '' },
    geoFenceId: { type: String, default: '' },
    site_name: { type: String, default: 'Your Site' },
    device_name: { type: String, default: 'Your Device' },

    timeStamp: { type: Number, default: 0 }
}, options);

module.exports = mongoose.model('tbl_smt400_geo', GeoSchema);