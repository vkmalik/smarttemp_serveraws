'use strict';
const uuid = require('uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {
  timestamps: true
};

const getRequiredFiledMessage = (filed) => {
    const message = `${filed} Should Not Be Empty`;
    return [true, message];
};

const ContactSchema = new Schema({
  id: { type: String, default: uuid },
  email: { type: String, required: getRequiredFiledMessage('Email'), trim: true },
  name: { type: String, default: 'user', trim: true },
  message: { type: String, default: '' }
}, options);


const Contact = mongoose.model('tbl_smt400_contact', ContactSchema);
module.exports = Contact;
