'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = { timestamps: true };


const requiredFieldMessage = (filed) => {
    let message = `${filed} Should Not Be Empty`;
    return [true, message]
 }


const ScheduleSchema = new Schema({
    mac: {type: String, required: requiredFieldMessage('Mac')},   
    fan_speed: {type: String, default: ''},
    //fan_type: {type: String, default: ''},
    temp_cool: {type: String, default: ''},
    name: {type: String, default: ''},
    temp_heat: {type: String, default: ''},
    scheduled: { type: String, default: ''},
    userId: { type: String, default: ''},
    // weekly: { type: Boolean, default: false},
    // daily: { type: Boolean, default: false}, 
    scheduleRunning: { type: Boolean, default: false },
    overRideRunning: { type: Boolean, default: false },
    days: [Number],
    scheduledDays: [Number],
    currentDay: { type: String, default: ''},
    time: { type: String, default: ''}, 
    enable_disable: { type: Boolean, default: true},
    uid: {type: String, default: ''}, 
    status: {type: String, default: 'pending'},
    overRide:  { type: Boolean, default: false},
    overRideTime:  { type: String, default: ''},
    scheduleTime: { type: Number, defult: 0 },
    history: { type: Array, defult: [] }
    }, options);






module.exports = mongoose.model('tbl_smt400_schedules', ScheduleSchema);

