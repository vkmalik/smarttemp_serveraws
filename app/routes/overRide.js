'use strict';
const express = require('express');
const router = express.Router();
const { Schedule, UserProfiles, DeviceParam, OverRide } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const { scheduleUpdateServ } = require('../lib');
const moment = require('moment-timezone');
const request = require('request-promise-native');

function getDateFunction() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let fullYear = date.getFullYear();
    let hours = date.getHours();
    let mini = date.getMinutes();
    let sec = date.getSeconds();
    let utcHours = date.getUTCHours();
    let utcCurrentDay = date.getUTCDay();
    return { day, date, month, fullYear, hours, mini, sec, utcHours, utcCurrentDay }
}


// to delete the schedule if days are empty

async function checkOverRideStatus(uid) {
    const result = await Schedule.find({ uid });
    if (result[0].days.length === 0) {
        await Schedule.findOneAndRemove({ uid }) // to delete previous added schedule as days are empty
    } else {
        await Schedule.findOneAndUpdate({ uid }, { scheduleRunning: true }, { new: true }); // to update previous added schedule
    }
}

// schedule update for overRide

router.post('/', async(req, res, next) => {
    const { mac, uid, overRideHr } = req.body || {};
    try {
        let { utcCurrentDay } = getDateFunction();
        if (!mac) {
            return res.json({ success: false, message: 'Mac Id Must' })
        } else {
            if (overRideHr === 'Next Event') {
                await Schedule.findOneAndUpdate({ uid }, { overRideTime: overRideHr, overRideRunning: true })
                await OverRide.findOneAndRemove({ mac }); // to delete override record

                let data = { mac, uid, utcCurrentDay, overRideHr }
                const overRide = new OverRide(data);
                overRide.save();
            } else if (overRideHr === 'Off') {
                await Schedule.updateMany({ mac }, { overRideRunning: false, overRideTime: '' }, { new: true })
                await OverRide.findOneAndRemove({ mac }); // to delete override record
                const result = await Schedule.findOne({ uid });
                const { temp_heat, temp_cool, fan_speed } = result;
                const finalResult = {
                    mac,
                    temp_heat,
                    temp_cool,
                    fan_speed
                };

                const MsgID = uuid(15);
                const finalResult1 = {
                    mac,
                    data: {
                        fan_speed,
                        temp_cool,
                        temp_heat,
                        MsgID
                    }
                };

                await request.post({ url: 'http://3.105.183.233:3000/SMT-400', json: true, body: finalResult });
                setTimeout(async function() {
                    await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult1 });
                    checkOverRideStatus(uid)
                }, 1000);
            } else {
                await OverRide.findOneAndRemove({ mac }); // to delete override record

                const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
                const getTimeZone = getDeviceResult.ipDetails.timezone;
                console.log(getTimeZone, 'timezone');

                //  for calculating Offset
                let { day, date, month, fullYear, hours, mini, sec, utcHours } = getDateFunction();

                if (month <= 9) {
                    month = '0' + month;
                    if (day <= 9) {
                        day = '0' + day;
                        if (hours <= 9) {
                            hours = '0' + hours;
                            if (mini <= 9) {
                                mini = '0' + mini;
                            }
                        }
                    } else {
                        if (hours <= 9) {
                            hours = '0' + hours;
                            if (mini <= 9) {
                                mini = '0' + mini;
                            }
                        } else {
                            if (mini <= 9) {
                                mini = '0' + mini;
                            }
                        }
                    }
                } else if (day <= 9) {
                    day = '0' + day;
                    if (hours <= 9) {
                        hours = '0' + hours;
                        if (mini <= 9) {
                            mini = '0' + mini;
                        }
                    } else {
                        if (mini <= 9) {
                            mini = '0' + mini;
                        }
                    }
                } else if (hours <= 9) {
                    hours = '0' + hours;
                    if (mini <= 9) {
                        mini = '0' + mini;
                    }
                } else {
                    if (mini <= 9) {
                        mini = '0' + mini;
                    }
                }


                var date1 = `${fullYear}-${month}-${day}`;
                var time = `${hours}:${mini}`;

                const timeZoneTime = moment.tz(`${date1} ${time}`, getTimeZone); // convert timezone
                const min = timeZoneTime.format().split(':');
                const getAddHour = timeZoneTime.format().split(':')[2].split('');

                var mins = parseFloat(min[min.length - 1] / 60);
                let hrs = overRideHr; // store the override time here
                var hr = `${getAddHour[3]}${getAddHour[4]}`;
                var hours1 = parseFloat(hrs) + parseFloat(hr) + mins;

                var offSet = `${getAddHour[2]}${hours1}`;

                // for calculating time based on timeZone using offset
                var d = new Date()
                var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
                var utcTime = new Date(utc + (3600000 * hrs)) // utc time as per override time
                var nd = new Date(utc + (3600000 * offSet)); // timezone time as per override time
                var updateUtcTime = utcTime.toLocaleString().split(', ')[1].split(':');
                var updatedTime = nd.toLocaleString().split(', ')[1].split(':');

                var savingUtcTime = `${updateUtcTime[0]}:${updateUtcTime[1]} ${updateUtcTime[2].split(' ')[1]}`
                var displayTimeNew = `${updatedTime[0]}:${updatedTime[1]} ${updatedTime[2].split(' ')[1]}`


                // calculating 24hr for savinig updated utc time
                var PM = savingUtcTime.match('PM') ? true : false
                var newtime = savingUtcTime.split(':')
                var minNew = newtime[1].split(' ')[0];
                var hour;
                if (PM) {
                    if (newtime[0] === '12') {
                        hour = newtime[0]
                    } else {
                        hour = 12 + parseInt(newtime[0], 10)
                    }
                } else {
                    if (newtime[0] === '12') {
                        newtime[0] = '00'
                        hour = newtime[0]
                    } else {
                        hour = newtime[0]
                    }
                }

                var currDay = 0;
                let addedHR = parseInt(utcHours) + parseInt(overRideHr);
                if (addedHR >= 24) {
                    currDay = parseInt(utcCurrentDay) + parseInt(1);
                    if (currDay > 6) {
                        currDay = 0;
                    }
                } else {
                    currDay = parseInt(utcCurrentDay)
                }

                var newUtcTime = minNew + ' ' + hour;
                var utcNew = hour + ' ' + minNew;

                var scheduled = `${newUtcTime} * * ${currDay}`

                let data = { overRideUTCTime: utcNew, time: displayTimeNew, scheduled, mac, uid, currentDay: currDay }
                await OverRide.findOneAndRemove({ mac }); // to delete previous override record for the same mac if exists
                const overRide = new OverRide(data);
                overRide.save();
                const test = await Schedule.findOneAndUpdate({ uid }, { overRideTime: displayTimeNew, overRideRunning: true, scheduleRunning: true })
                scheduleUpdateServ.overRide(scheduled, uid);
            }
            return res.json({ success: true, message: "Success" });

        }

    } catch (error) {
        next(error);
    }
})


module.exports = router;