'use strict';

const express = require('express');
const router = express.Router();
const uuid = require('uuid');

const { User, UserProfiles, DeviceParam, Schedule } = require('../../models');
const { LocalProfile } = UserProfiles;
const { PasswordServ, TokenServ } = require('../../lib');

const {
    NewAccountVerification
} = require('../../helpers/mail');


const sendVarificationEmail = async(profile, email) => {
    const otp = uuid();
    const otpToken = TokenServ.generate({ otp });

    try {
        profile.otp = otp;
        await profile.save();
        await NewAccountVerification.send(await otpToken, email);
    } catch (error) {
        console.error(error);
    }
};


router.route('/profiles')

.get(async(req, res, next) => {
    try {
        const user = await LocalProfile.find({}).exec();
        res.json({ user })
    } catch (err) {
        next(err)
    }
})


router.route('/profiles/:id')

.get(async(req, res, next) => {
    const { id } = req.params;
    try {
        const user = await DeviceParam.find({ mac: id }).exec();
        res.json({ user })
    } catch (err) {
        next(err)
    }
})

router.route('/params')


/**
 * Login along with 28 parameters
 * 
 */

.post(async(req, res, next) => {

    const {
        email,
        password
    } = req.body;

    try {
        const user = await User.findOne({ email }).exec();

        if (!user) {
            const errorData = 'User does not exist'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 400;
            return next(error);
        }

        if (user && !user.isActive) {
            const errorData = 'Your account has been disabled. Please contact your system Administrator'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 400;
            return next(error);
        }

        const profile = await LocalProfile.findOne({ userId: user.id }).exec();

        if (!profile.isEmailVerified) {
            sendVarificationEmail(profile, email);
            //const error = new Error('You Should Verify Your Email ID To Login');
            const errorData = 'You should verify your email ID to login'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 401;
            return next(error);
        }

        const isCorrectPassword = await PasswordServ.match(password, profile.password);

        if (!isCorrectPassword) {
            const errorData = 'Incorrect Password'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
        }

        const tokenData = {
            email,
            provider: profile.provider,
            userId: user.id,
            profileId: profile.id
        };

        const { mac } = profile;
        const results = await Promise.all(mac.map(id => DeviceParam.find({ mac: id }).exec()));
        const deviceData = results.map(result => result.reverse()[0]);


        //            const token = await TokenServ.generate(tokenData);


        Object.assign(profile, { mac: deviceData })
        const finalData = { success: true, payload: profile, isupdateavailable : true, isavailableversioncode : 6  }
        res.json(finalData);




    } catch (error) {
        next(error);
    }

})


router.route('/params/:id')

.get(async(req, res, next) => {
        const { id } = req.params || {};
    try {
        const user = await User.find({id}).exec();
        if( user ){
                const users = await User.find({}).exec();
                res.json({ users })
        }
    } catch (err) {
        next(err)
    }
})



router.route('/')


/**
 * Login without 28 parameters
 * 
 */

.post(async(req, res, next) => {

    const {
        email,
        password
    } = req.body;

    try {
        const user = await User.findOne({ email }).exec();

        if (!user) {
            const errorData = 'User does not exist'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 400;
            return next(error);
        }

        if (user && !user.isActive) {
            const errorData = 'Your account has been disabled. Please contact your system Administrator'
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 400;
            return next(error);
        }

        const profile = await LocalProfile.findOne({ userId: user.id }).exec();

        if (!profile.isEmailVerified) {
            sendVarificationEmail(profile, email);
            //const error = new Error('You Should Verify Your Email ID To Login');
            const errorData = { message: 'You should verify your email ID to login' };
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
            error.status = 401;
            return next(error);
        }

        const isCorrectPassword = await PasswordServ.match(password, profile.password);

        if (!isCorrectPassword) {
            const errorData = { message: 'Incorrect Password' };
            const finalData = { success: false, payload: errorData }
            return res.json(finalData);
        }

        const tokenData = {
            email,
            provider: profile.provider,
            userId: user.id,
            profileId: profile.id
        };

        const { mac } = profile


        //          const token = await TokenServ.generate(tokenData);
        const finalData = { success: true, payload: profile, isupdateavailable : true, isavailableversioncode : 6  }
        res.json(finalData);

    } catch (error) {
        next(error);
    }

})


router.route('/:id')

.get(async(req, res, next) => {
    const { id } = req.params;
    const user = await User.findOne({ id }).exec();

    if (!user) {
        const errorData = 'User does not exist'
        const finalData = { success: false, payload: errorData }
        return res.json(finalData);
        error.status = 400;
        return next(error);
    }

    if (user && !user.isActive) {
        const errorData = 'Your account has been disabled. Please contact your system Administrator'
        const finalData = { success: false, payload: errorData }
        return res.json(finalData);
        error.status = 400;
        return next(error);
    }
    const profile = await LocalProfile.findOne({ userId: id }).exec();
    const { mac } = profile;
    const results = await Promise.all(mac.map(id => DeviceParam.find({ mac: id }).exec()));
    const scheduleResults = await Promise.all(mac.map(id => Schedule.find({ mac: id, status: 'pending' }).sort({ scheduleTime: 1 })));
    const deviceData = results.map(result => result.reverse()[0]);


    // const finalData = { success: true, payload: deviceData }
    res.json({ macs: deviceData, schedules: scheduleResults, success: true, isupdateavailable : true, isavailableversioncode : 6 });
})


router.route('/userUpdate')
    .put(async(req, res, next) => {
        const {
            email,
            state
        } = req.body;
        try {
            const result = await User.findOneAndUpdate({ email }, { $set: { isActive: state } }, { new: true });
            res.json(result)
        } catch (err) {
            next(err)
        }
    })

module.exports = router;
