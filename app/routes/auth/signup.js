'use strict';

const uuid = require('uuid');
const express = require('express');
const router = express.Router();
const { User, UserProfiles } = require('../../models');
const { LocalProfile } = UserProfiles;
const { PasswordServ, TokenServ } = require('../../lib');
const { URLSearchParams, URL } = require('url');
const config = require('config');
const HOST_ADDR = config.get('HOST_ADDR');

var fs = require('fs');

const {
    NewAccountVerification
} = require('../../helpers/mail');

const inValidLinkError = (next, message = 'Invalid Link') => {
    const error = new Error(message);
    error.status = 400;
    const errorData = "Invalid Link"
    const finalData = { success: true, payload: errorData }
    return next(finalData);
};

const getRedirectURL = (paramsObj = {}, targetURL) => {
    const params = new URLSearchParams(paramsObj);
    const url = new URL(targetURL);
    url.search = params.toString();
    return url;
};

const errorRedirect = (message, res) => {
    const targetURL = `${HOST_ADDR.server}`;
    //    const targetURL = `${HOST_ADDR.server}/api/auth/signup`;
    const url = getRedirectURL({ message }, targetURL);
    //    res.redirect(`${HOST_ADDR.server}`);
    res.redirect(url);
};

router.route('/')

.get(async(req, res, next) => {
    const { verify, id } = req.query;

    console.log(verify, id)
    if (!(verify === 'account') || !id) {
        return inValidLinkError(next);
    }

    try {
        const user = await LocalProfile.findOne({ userId: id }).exec();
        if (user) {
            await LocalProfile.findOneAndUpdate({ userId: id }, { isEmailVerified: true }, { new: true }).exec();
            const targetURL = `${HOST_ADDR.server}`;
            res.redirect(targetURL);
        }
    } catch (error) {
        const message = 'Expired';
        errorRedirect(message, res);
    }


})


/**
 * Register New User
 */

.post(async(req, res, next) => {
    const { body } = req;
    const {
        email,
        firstName,
        surName,
        role
    } = body;
    const query = {
        email: email.toLowerCase()
    };


    try {
        if (email == undefined || firstName == undefined || surName == undefined || email === "" || firstName === "" || surName === "") {
            const errorData = { message: 'All fields are mandatory' };
            const finalData = { success: false, payload: errorData }
            return res.json({ finalData })
        }
        const user = await User.findOne(query).exec();

        if (user) {
            const error = new Error('User Already Exist.. Please Login..!');
            error.status = 409;
            throw error;
        }

        const newUser = new User({
            email,
            role
        });

        const result = await newUser.save();
        const userId = result.id;
        const password = await PasswordServ.hash(body.password);
        const otp = uuid();
        const profileData = {
            userId,
            firstName,
            surName,
            password,
            otp
        };

        // const otpToken = TokenServ.generate({ otp });
        const profile = new LocalProfile(profileData);
        await profile.save();
        // NewAccountVerification.send(await otpToken, email, firstName)
        NewAccountVerification.send(userId, email.toLowerCase(), firstName)
            .then(data => {})
            .catch(error => console.error(error));
        const message1 = 'Verification Email Sent To Your Email Id.. Please Verify Your Email By Clicking The Verification Link'
        const payload = { success: true, payload: message1 }
        res.json(
            payload
        );

    } catch (error) {
        const errordata = 'User Already Exist.. Please Login..!'
        const finalData = { success: false, payload: errordata }
        res.json(finalData);
        next(error);
    }

})



router.route('/sendMail')
    .post(async(req, res, next) => {
        const { body } = req;
        const {
            email,
            id
        } = body;
        const query = {
            email: email.toLowerCase(),
            id
        };
        try {
            const user = await User.findOne(query).exec();
            if (user) {
                const userId = id;
                const profile = await LocalProfile.findOne({ userId: id })
                if (profile) {
                    const firstName = profile.firstName;
                    NewAccountVerification.send(userId, email.toLowerCase(), firstName)
                        .then(data => {})
                        .catch(error => console.error(error));

                    res.json({ msg: 'success' })
                }
            } else {
                const error = new Error('User Not Found!');
                error.status = 409;
                throw error;
            }
        } catch (err) {
            next(err)
        }
    })



router.route('/sendActivationMail')
    .post(async(req, res, next) => {
        const { body } = req;
        const {
            email,
            id
        } = body;
        const query = {
            email: email.toLowerCase(),
            id
        };
        try {
            const user = await User.findOne(query).exec();
            if (user) {
                const userId = id;
                const profile = await LocalProfile.findOne({ userId: id })
                if (profile) {
                    await LocalProfile.findOneAndUpdate({ userId: id }, { isEmailVerified: true }, { new: true })

                    const firstName = profile.firstName;
                    NewAccountVerification.send(email.toLowerCase(), firstName)
                        .then(data => {})
                        .catch(error => console.error(error));

                    res.json({ msg: 'success' })
                }
            } else {
                const error = new Error('User Not Found!');
                error.status = 409;
                throw error;
            }
        } catch (err) {
            next(err)
        }
    })

router.post('/createUsers', async(req, res, next) => {
    try {
        let jsonData = {}
        fs.readFile('tbl_smt400_users.json', 'utf-8', (err, data) => {
            if (err) throw err
            let count = 0;
            jsonData = JSON.parse(data)
            jsonData.forEach(async(ele, i) => {
                const newUser = new User({
                    email: ele.email
                });

                const result = await newUser.save();
                return;
            })
        })
    } catch (err) {
        next(err)
    }
})

router.post('/createLocalProfiles', async(req, res, next) => {
    try {
        let jsonData = {}
        fs.readFile('users.json', 'utf-8', async(err, data) => {
            if (err) throw err
            let count = 0;
            jsonData = JSON.parse(data)
            const user = await User.find().exec();
            user.forEach(async(ele1) => {
                let noOfDevice = 0;
                let mac = [];
                jsonData.forEach(async(ele, i) => {
                    if (ele.login_name === ele1.email) {
                        noOfDevice++;
                        mac.push(ele.mac)
                    }
                })
                const userId = ele1.id;
                const password = await PasswordServ.hash('smarttemp');
                const profileData = {
                    userId,
                    firstName: ele1.email,
                    password,
                    mac,
                    noOfDevice,
                    surName: ele1.email,
                    isEmailVerified: true
                };
                const userProfile = await LocalProfile.findOne({ userId }).exec();
                if (!userProfile) {
                    const profile = new LocalProfile(profileData);
                    await profile.save();
                } else {
                    await LocalProfile.findOneAndUpdate({ userId }, { isEmailVerified: true }, { new: true })
                }
            })
        })
    } catch (err) {
        next(err)
    }
})


router.post('/updateJohn', async(req, res, next) => {
    try {
        let userId = '9865bc68-fcfc-43d5-80b8-5e49a43026a4'
        const userProfile = await LocalProfile.findOne({ userId }).exec();
        if (userProfile) {
            let mac = [
                "F0FE6B765D0A",
                "F0FE6BB938F2",
                "F0FE6B7665E4",
                "F0FE6B76643C",
                "F0FE6B76670E",
                "F0FE6B765F4A"
            ];
            let count = 6;
            await LocalProfile.findOneAndUpdate({ userId }, { mac: mac, noOfDevice: count }, { new: true }).exec();
        }
    } catch (err) {
        next(err)
    }
})

module.exports = router;