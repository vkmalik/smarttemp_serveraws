'use strict';
const express = require('express');
const router = express.Router();

const { User, UserProfiles, DeviceParam } = require('../../models');
const { LocalProfile } = UserProfiles;
router.route('/')

/*
 * Delete Account
 * 
 */


.post(async(req, res, next) => {
    const { email, id } = req.body;
    try {
        const user = await User.findOneAndUpdate({ email, id }, { $set: { isDeleted: true } })
        if (!user) {
            const error = new Error('User Not Exist');
            error.status = 400;
            return next(error);
        }
        res.json({ message: "User Will Deleted Successfully" })
    } catch (error) {
        next(error)
    }
})

.put(async(req, res, next) => {
    const { email, userId } = req.body;
    try {
        let count = 0;
        let count1 = 0;
        // const user = await User.findOneAndRemove({ email }).exec()
        let exist = false;
        const userprofile = await LocalProfile.findOne({ userId }).exec()
        const mac = userprofile.mac;
        if (mac.length > 0) {
            const profiles = await LocalProfile.find().exec()
            mac.forEach(async(ele2, i) => {
                count = i + 1;
                profiles.forEach(ele => {
                    ele.mac.forEach(ele1 => {
                        if (ele1 === ele2) {
                            // exist = true;
                            count1 = count1 + 1;
                        }
                    })
                })
                if (count1 === 1) {
                    await DeviceParam.findOneAndRemove({ mac: ele2 }, { new: true }).exec()
                }
                // exist = false;
            })
            if (mac.length === count) {
                await LocalProfile.findOneAndRemove({ userId }).exec()
                await User.findOneAndRemove({ email }).exec()
                res.json({ 'message': 'success' })
            }
        } else {
            await LocalProfile.findOneAndRemove({ userId }).exec()
            await User.findOneAndRemove({ email }).exec()
            res.json({ 'message': 'success' })
        }
    } catch (err) {
        next(err)
    }
})


module.exports = router;