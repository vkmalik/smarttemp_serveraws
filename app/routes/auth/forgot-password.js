'use strict';

const uuid = require('uuid');
const express = require('express');
const router = express.Router();

const { User, UserProfiles } = require('../../models');
const { LocalProfile } = UserProfiles;
const { PasswordServ, TokenServ } = require('../../lib');

const {
    SendPasswordResetMail
} = require('../../helpers/mail');


router.route('/')


    /**
     * Login
     * 
     */

    .post(async(req, res, next) => {

        const {
            email
        } = req.body;

        try {
            const user = await User.findOne({ email }).exec();

            if (!user) {
                //const error = new Error('User Not Exist');
                //error.status = 400;
                //return next(error);
                const error = { message: 'User does not exist'};
                const errorData = { success: false, payload: error }
                return res.json(errorData);
                //error.status = 400;
            }

            const otp = Math.floor(Math.random() * 10000)

            const profile = await LocalProfile.findOne({ userId: user.id }).exec();
            await Object.assign(profile, { otp }).save();
             SendPasswordResetMail.send(await otp, email, profile.firstName)
//		SendPasswordResetMail.send(await otp, 'niharika@veniteck.com', profile.firstName)
                .then(data => {})
                .catch(error => console.error(error));
                const message = 'OTP has been sent to your Email. Please use the OTP to reset your password'
                const finalData = { success: true, payload: message }
                res.json({finalData});


        } catch (error) {
            next(error);
        }

    })
/**
     * User Change Password
     */
    
.put(async(req, res, next) => {

        const {
            userId,
            password,
            conPassword
        } = req.body;


        try {
		let flag = req.body.flag;
		if(flag !== true){
			flag = false;
		}
            //const user = await LocalProfile.findOne({ userId }).exec();

            //if (!user) {
                //const error = new Error('User Not Exist');
                //error.status = 400;
               // const message = 'User Not Exist'
                //const errorData = { success: false, payload: message }
               // res.json(errorData);
                //return next(error);
            //}

            const profile = await LocalProfile.findOne({ userId }).exec();
	   if(!flag){
            const isCorrectPassword = await PasswordServ.match(password, profile.password);
             if (!isCorrectPassword) {
                //const error = new Error('Incorrect Password');
                //error.status = 401;
                const message = 'Incorrect Password'
                const finalData = { success: false, payload: message }
                res.json(finalData);
                //return next(error);
            }
	   }

	    console.log(conPassword, 'Pass');
            const hashPassword = await PasswordServ.hash(conPassword);
            await Object.assign(profile, { password: hashPassword }).save();
            const message = 'Password Updated Successfully'
            const finalData = { success: true, payload: message }
            res.json({
                finalData
            });


        } catch (error) {
            next(error);
        }

    })




module.exports = router;

