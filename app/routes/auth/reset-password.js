'use strict';

const uuid = require('uuid');
const express = require('express');
const router = express.Router();

const { User, UserProfiles } = require('../../models');
const { LocalProfile } = UserProfiles;
const { PasswordServ, TokenServ } = require('../../lib');

router.route('/')


    /**
     * Login
     * 
     */

    .post(async(req, res, next) => {
        const { 
            otp,
            password: plainPassword
        } = req.body;

        const updateObj = {
            otp: undefined,
            isEmailVerified: true
        };

        try {
           // const { otp } = await TokenServ.verify(otpToken);
            const user = await LocalProfile.findOne({ otp }).exec();
            if (!user || !user.otp) {
                //const message = 'Invalid OTP Entry';
                //return inValidLinkError(next, message);
                const error = { message: 'Invalid Authentication Code'};
                const errorData = { success: false, payload: error }
                return res.json(errorData);
                //error.status = 409;
                //throw error;
            }

            const password = await PasswordServ.hash(plainPassword);
            Object.assign(user, updateObj, { password });
            await user.save();
            //res.json({ message: 'Password Updated Successfully' });
            const message = 'Password Updated Successfully'
            const finalData = { success: true, payload: message }
            res.json({finalData});

        } catch (error) {
            next(error);
        }
    })



module.exports = router;

