'use strict';
const express = require('express');
const router = express.Router();
const { DeviceParam, UserProfiles } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');

// mob or web or device Insert Value

router.post('/', async(req, res) => {
    const { mac, userId, pair_key } = req.body;
    try {
        if (pair_key) {
            const CheckPairKey = await DeviceParam.findOne({ pair_key, mac });
            if (!CheckPairKey) {
                const message = 'Authentication Failed'
                const finalData = { success: false, payload: message }
                return res.json({ finalData });
            }
        }
        const result = await LocalProfile.find({ userId });
        if (result[0].mac.includes(mac)) {
            const message = 'This Mac Id Already Exist'
            const finalData = { success: false, payload: message }
            return res.json({ finalData });
        }
        if (!result[0]) {
            const error = new Error('Bad Request');
            error.status = 400;
            return next(error);
        }
        const newMac = result[0].mac;
        newMac.push(mac)
        const noOfDevice = result[0].mac.length;
        var result1 = await LocalProfile.findOne({ userId });
        const res2 = await DeviceParam.findOne({ mac })
        var success = true;
        if (!res2) {
            success = false;
        }
        if (success) {
            result1 = await LocalProfile.findOneAndUpdate({ userId }, { $set: { mac: newMac, noOfDevice } }, { new: true });
        }
        const finalData = { success, payload: result1 }
        return res.json(finalData)

    } catch (error) {
        res.json(error);
    }
});


// Edit Name mac id from account
router.put('/', async(req, res) => {
    const { mac, site_name, device_name } = req.body;
    try {
        const res2 = await DeviceParam.find({ mac }).sort();
        const res1 = res2.reverse()[0];
        let a = [{ 'date': new Date(), 'from': 'deviceList' }]
        const result = await DeviceParam.findOneAndUpdate({ mac, updatedAt: res1.updatedAt }, { updatedFrom: a, site_name, device_name }, { new: true });
        const finalData = { success: true, payload: result }
        return res.json({ finalData })

    } catch (error) {
        res.json(error)
    }
});

// deleting mac id from account

router.delete('/', async(req, res) => {
    const { mac, userId } = req.body;
    try {

        const result = await LocalProfile.find({ userId });
        if (!result[0]) {
            const error = new Error('Bad Request');
            error.status = 400;
            return next(error);
        }
        const newMac = result[0].mac
        const getIndex = newMac.indexOf(mac);
        if (getIndex === -1) {
            const message = 'This Mac Id Not Match'
            const finalData = { success: false, payload: message }
            throw res.json({ finalData })
        }
        result[0].mac.splice(getIndex, 1);
        const noOfDevice = result[0].mac.length;
        const result1 = await LocalProfile.findOneAndUpdate({ userId }, { $set: { mac: newMac, noOfDevice } }, { new: true });
        const finalData = { success: true, payload: result1 }
        return res.json(finalData)

    } catch (error) {
        res.json(error);
    }
});


router.route('/portalDelete')
    .put(async(req, res, next) => {
        const { mac, userId } = req.body;
        try {
            const result = await LocalProfile.find({ userId });
            if (!result[0]) {
                const error = new Error('Bad Request');
                error.status = 400;
                return next(error);
            }
            const newMac = result[0].mac
            const getIndex = newMac.indexOf(mac);
            if (getIndex === -1) {
                const message = 'This Mac Id Not Match'
                const finalData = { success: false, payload: message }
                throw res.json({ finalData })
            }
            result[0].mac.splice(getIndex, 1);
            const noOfDevice = result[0].mac.length;
            const result1 = await LocalProfile.findOneAndUpdate({ userId }, { $set: { mac: newMac, noOfDevice } }, { new: true });
            const finalData = { success: true, payload: result1 }
            return res.json(finalData)
        } catch (err) {
            next(err)
        }
    })

// mob or web or device get Value

router.get('/:id', async(req, res) => {
    try {
        const id = req.params.id;
        const result = await LocalProfile.find({ mac: id });
        const data = result.reverse()[0];
        res.json(data);
    } catch (error) {
        res.json(error);
    }
});

/**
 * Get All Device List in Admin
 */

router.get('/', async(req, res) => {
    try {
        const result = await LocalProfile.find({});
        res.json(result);
    } catch (error) {
        res.json(error);
    }
});

module.exports = router;