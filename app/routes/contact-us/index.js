'use strict';
const express = require('express');
const router = express.Router();
const { Contact } = require('../../models');
const {
    sendContact
} = require('../../helpers/mail');


router.route('/')

    .post(async(req, res, next) => {
        const { email, name, message } = req.body
        try {
            const contact = new Contact(req.body);
            await contact.save();
            sendContact.send(await { email, name, message })
                .then(data => {})
                .catch(error => console.error(error))
            res.json({ message: "mail sent successfully" })
        } catch (error) {
            next(error);
        }
    })


module.exports = router;