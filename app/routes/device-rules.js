'use strict';
const express = require('express');
const router = express.Router();
const { DeviceRules } = require('../models');

// Add Device Rules

router.post('/', async (req, res) => {
    const { mac } = req.body || {};
    try {
        if (!mac) {
            const message = 'Authentication Failed'
            const finalData = { success: false, payload: message }
            return res.json({ finalData });
        }
        const deviceRules = new DeviceRules(req.body);
        const result = await deviceRules.save();
        res.json(result)
    } catch (error) {
        res.json(error);
    }
});




// Edit Device Rules
router.put('/:id', async (req, res) => {
    const { id } = req.params || {};
    try {
        const result = await DeviceRules.findOneAndUpdate({ id }, req.body, { new: true });
        return res.json(result)

    } catch (error) {
        res.json(error)
    }
});

// deleting  Device Rules

router.delete('/:id', async (req, res) => {
    const { id } = req.params || {};
    try {
        const result = await DeviceRules.findOneAndRemove({ id });
        return res.json(result)
    } catch (error) {
        res.json(error);
    }
});

// Get Device Rules Value

router.get('/:mac', async (req, res) => {
    try {
        const { mac } = req.params;
        const result = await DeviceRules.find({ mac });
        res.json(result);
    } catch (error) {
        res.json(error);
    }
});


module.exports = router;
