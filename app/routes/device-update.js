'use strict';
const express = require('express');
const router = express.Router();
const { DeviceParam, DeviceParamHistory, Schedule, User, UserProfiles } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const request = require('request-promise-native');
const moment = require('moment-timezone');
var fs = require('fs');

/**
 * mob or web or device Insert Value and Update Value
 */

// async function checkDST() {
//     var date = new Date()
//     moment().isDST();
// }


(async function() {
    try {
        console.log("hello")

        let interval = 1000 * 60 * 60 * 24;
        setInterval(async() => {
            const result = await DeviceParamHistory.find({}).exec();
            result.forEach(async(ele) => {
                var recDate = ele.createdAt;
                const diffTime = Math.abs(dt - recDate);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                if (diffDays > 7) {
                    console.log('History Deleted', ele.mac)
                    await DeviceParamHistory.findOneAndRemove({ _id: ele._id })
                }
            })
        }, interval)



        setInterval(async() => {
            const res = await DeviceParam.find({});
            var date = new Date();
            var timestamp = date.getTime();
            var newTimeStampInSec = parseInt(timestamp / 1000)
            var falseArray = new Array();
            res.forEach(async(ele) => {

                if ((newTimeStampInSec - ele.timeStamp) > 180) {
                    const MsgID = uuid(15);
                    const finalResult = {
                        mac: ele.mac,
                        data: {
                            MsgID,
                            temp_cool: ele.temp_cool,
                            temp_heat: ele.temp_heat
                        }
                    };

                    const result = await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
                }

                if ((newTimeStampInSec - ele.timeStamp) > 300) {
                    // get livedata mac detail for this mac
                    // fire that packet/data to thermostat

                    const MsgID = uuid(15);
                    const finalResult = {
                        mac: ele.mac,
                        data: {
                            MsgID,
                            temp_cool: ele.temp_cool,
                            temp_heat: ele.temp_heat
                        }
                    };

                    let a = [{ 'date': new Date(), 'from': 'deviceUpdateAWSAutoLoop' }]
                    await DeviceParam.findOneAndUpdate({ mac: ele.mac }, { updatedFrom: a, isOnline: false }, { new: true });
                }
            })
        }, 120000);
    } catch (err) {
        return err
    }

}())


router.post('/timeStamp', async(req, res, next) => {
    const { body } = req;
    try {
        const { timestamp, mac } = body || {};
        let a = [{ 'date': new Date(), 'from': 'deviceUpdateAWSTimeStamp' }]
        const result = await DeviceParam.findOneAndUpdate({ mac }, { updatedFrom: a, timeStamp: timestamp }, { new: true });
        res.json(result)
    } catch (err) {
        next(err)
    }
})


router.post('/deviceStatus', async(req, res, next) => {
    const { body } = req;
    try {
        const { socketName } = body || {}
        console.log(socketName, 'device status')
        let a = [{ 'date': new Date(), 'from': 'deviceUpdateAWSdeviceStatus' }]
        const result = await DeviceParam.findOneAndUpdate({ socketIp: socketName }, { updatedFrom: a, isOnline: false }, { new: true });
        res.json(result)
    } catch (err) {
        next(err)
    }
})


router.post('/', async(req, res) => {
    const controlParameters = [
        'equip_mode',
        'fan_speed',
        'temp_cool',
        'temp_heat',
        'occupied',
        'relay_status',
        'wifi_lost',
        'fan_type'
    ];
    const { mac, MsgID, temp_cool, temp_heat } = req.body;
    try {
        // console.log(temp_cool, temp_heat, mac)
        // New device 

        //function changeValue(num) {
        //const number = num.toString();
        //const val = number.split('.');
        // var n;
        //if (val[1][0] < 5) {
        //  n = val[0]
        //return n;
        //}
        //n = val[0] + '.' + "5"
        // return n
        //}
        function changeValue(num) {
            var bo = num;
            // console.log(num)
            if (bo.indexOf(".") >= 0) {
                var arr = bo.split(".")
                var nu = arr[0];
                var dec = arr[1];
                if (dec.toString().charAt(0) == "0") {
                    return Number(nu)
                } else if (dec.toString().charAt(0) > "5") {
                    return Number(nu) + 1
                } else if (dec.toString().charAt(0) < "5") {
                    return Number(nu)
                } else {
                    if (dec.toString().length > 1) {
                        if (dec.charAt(1) == "0") {
                            return Number(nu) + ".5"
                        } else {
                            return Number(nu) + 1
                        }
                    } else {
                        return Number(nu) + ".5"
                    }
                }
                //return "Whole No:"+nu.length+"\t\tDec:"+dec.length)
            } else {
                return Number(num)
                    // console.log("its whole no:"+num)
            }
        }
        if (temp_cool !== undefined && temp_cool !== 'off') {
            const temp_cool1 = changeValue(temp_cool)
            Object.assign(req.body, { temp_cool: temp_cool1 })
        }
        if (temp_heat !== undefined && temp_heat !== 'off') {
            const temp_heat2 = changeValue(temp_heat)
            Object.assign(req.body, { temp_heat: temp_heat2 })
        }



        const findValue = await DeviceParam.findOne({ mac });
        if (!findValue) {
            const deviceParam = new DeviceParam(req.body);
            const historySchema = new DeviceParamHistory(req.body);
            await deviceParam.save();
            await historySchema.save();
            return res.json({ result: "ok" })
        }

        // Compare Device Value And Collection Values

        const MsgID = uuid(15);
        let a = [{ 'date': new Date(), 'from': 'deviceUpdateAWS' }]
        Object.assign(req.body, { MsgID, updatedFrom: a })

        const result = await DeviceParam.findOneAndUpdate({ mac }, req.body, { new: true });
        const historySchema = new DeviceParamHistory(req.body);
        await historySchema.save();
        const finalData = { success: true, payload: result }
        res.json(finalData);
        const finalResult = {
            mac,
            data: {
                MsgID
            }
        };
        controlParameters.forEach(async(key) => {
            if (findValue[key] !== result[key]) {
                finalResult.data[key] = result[key];
                if (key === 'temp_cool' || key === 'temp_heat' || key === 'fan_speed') {
                    await Schedule.updateMany({ mac, scheduleRunning: true }, { scheduleRunning: false })
                }
            }
        });
        await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
        var dt = new Date();
        const result1 = await DeviceParamHistory.find({ mac: result.mac });
        result1.forEach(async(ele) => {
            var recDate = ele.createdAt;
            //console.log(result1.length, recDate)
            const diffTime = Math.abs(dt - recDate);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            //console.log(diffDays)
            if (diffDays > 7) {
                //  console.log(diffDays,'time elapsed')
                await DeviceParamHistory.findOneAndRemove({ _id: ele._id })
            }
        })
        return;
    } catch (error) {
        res.json(error);
    }
});



/**
 * mob or web or device get Value
 */

router.get('/:id', async(req, res) => {
    try {
        const mac = req.params.id;
        const result = await DeviceParam.findOne({ mac });
        //const finalData = { success: true, payload: result }
        res.json(result);

    } catch (error) {
        res.json(error);
    }
});

router.get('/history/:id', async(req, res) => {
    try {
        const mac = req.params.id;
        const result = await DeviceParamHistory.find({ mac }).sort({ _id: -1 }).limit(500);
        //const finalData = { success: true, payload: result }
        res.json(result);

    } catch (error) {
        res.json(error);
    }
});


router.post('/createDeviceDetails', async(req, res) => {
    try {
        let jsonData = {}
        fs.readFile('mac.json', 'utf-8', (err, data) => {
            if (err) throw err
            let count = 0;
            jsonData = JSON.parse(data)
            jsonData.forEach(async(ele, i) => {
                const mac = ele.mac;
                const site_name = ele.site_name;
                const device_name = ele.device_name;

                const profileData = {
                    site_name,
                    mac,
                    device_name
                };

                const deviceParam = new DeviceParam(profileData);
                await deviceParam.save();
            })
        })
    } catch (err) {
        next(err)
    }
})

router.post('/updateDeviceDetails', async(req, res) => {
    try {
        const result = await DeviceParam.find({});
        result.forEach(async(ele, i) => {
            let mac = '';
            mac = ele.mac;
            if (ele.site_name === '') {
                ele.site_name = 'Your Site'
                console.log('site', mac, ele.site_name);
                await DeviceParam.findOneAndUpdate({ mac: mac }, { site_name: ele.site_name }, { new: true })
            } else if (ele.device_name === '') {
                ele.device_name = 'Your Device'
                console.log('device', mac, ele.device_name)
                await DeviceParam.findOneAndUpdate({ mac: mac }, { device_name: ele.device_name }, { new: true })

            } else {
                console.log('not Empty')
            }
            //await DeviceParam.findOneAndUpdate({ mac:mac }, {site_name:ele.site_name, device_name:ele.device_name}, { new: true })
        })
        res.json("success")
    } catch (err) {
        next(err)
    }
})


router.post('/checkDuplicate', async(req, res) => {
    try {
        const result = await DeviceParam.find({})
        result.forEach(async(ele, i) => {
            result.forEach(async(ele1, j) => {
                if (ele.mac === ele1.mac) {
                    if (ele1.site_name === '' || ele1.device_name === '') {
                        console.log('Match Found', ele1.mac)
                    } else {
                        console.log('No Match')
                    }
                }
            })
        })
    } catch (err) {
        next(err)
    }
})

router.route('/timezoneupdate/:id')
    .put(async(req, res, next) => {
        const { timezoneNew } = req.body || {}
        const { id } = req.params
        try {
            const result = await DeviceParam.findOneAndUpdate({ mac: id }, {
                $set: {
                    "ipDetails.timezone": timezoneNew
                }
            }, { new: true }).exec()

            const result1 = await Schedule.find({ mac: id }).exec()
            result1.forEach(async(ele1) => {
                var PM = ele1.time.match('PM') ? true : false
                var newtime = ele1.time.split(':')
                var minNew = newtime[1].split(' ')[0];
                var hour;
                if (PM) {
                    if (newtime[0] === '12') {
                        hour = newtime[0]
                    } else {
                        hour = 12 + parseInt(newtime[0], 10)
                    }
                } else {
                    if (newtime[0] === '12') {
                        newtime[0] = '00'
                        hour = newtime[0]
                    } else {
                        hour = newtime[0]
                    }
                }

                var newTime = hour + ':' + minNew;

                var date = new Date();
                var month = date.getMonth() + 1;
                var day = date.getDate();

                if (month <= 9) {
                    month = '0' + month;
                    if (day <= 9) {
                        day = '0' + day;
                    }
                } else if (day <= 9) {
                    day = '0' + day;
                }

                var dt = date.getFullYear() + '-' + month + '-' + day;
                console.log(result.ipDetails.timezone, 'check time zone')
                const getTimeZone = result.ipDetails.timezone


                let a = ele1.scheduled.split(' ');
                const timeSplit = newTime.split(':');
                var getTime = timeSplit[0];
                const getHoure1 = getTime.length === 1 ? `0${getTime}` : getTime;

                const timeZoneTime = moment.tz(`${dt} ${getHoure1}:${timeSplit[1]}`, getTimeZone); // convert timezone

                const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
                const d = timeZoneTimeHu[1].split(':');
                a[1] = d[1];
                a[2] = d[0];
                let c = a.join(' ');

                await Schedule.findOneAndUpdate({ uid: ele1.uid }, { scheduled: c }, { new: true }).exec()
            })

            res.json(result)

        } catch (err) {
            next(err)
        }
    })

router.route('/portalUpdate/:id')
    .put(async(req, res, next) => {
        //  console.log(req.body, 'device update', req.params)
        const { heat, cool, equip, fan, siteName, deviceName } = req.body || {}
        const { id } = req.params;
        try {
            const MsgID = uuid(15);
            const finalResult = {
                mac: id,
                data: {
                    MsgID,
                    temp_heat: heat,
                    temp_cool: cool,
                    equip_mode: equip,
                    fan_speed: fan
                }
            };
            const result = await DeviceParam.findOneAndUpdate({ mac: id }, { temp_heat: heat, temp_cool: cool, equip_mode: equip, fan_speed: fan, site_name: siteName, device_name: deviceName }).exec()
            if (result) {
                const result1 = await request.post({ url: 'http://13.238.58.241:8000', json: true, body: finalResult });
                res.json(result)
            }
            //res.json(result)
        } catch (err) {
            next(err)
        }
    })



router.route('/updateSchedules')
    .put(async(req, res, next) => {
        try {
            const result = await DeviceParam.find({}).lean()
            result.forEach(async(ele) => {
                if (ele.ipDetails !== undefined) {
                    const result1 = await Schedule.find({ mac: ele.mac }).exec()
                    result1.forEach(async(ele1) => {
                        console.log(ele1.scheduled, 'existing Schedule')
                        var PM = ele1.time.match('PM') ? true : false
                        var newtime = ele1.time.split(':')
                        var minNew = newtime[1].split(' ')[0];
                        var hour;
                        if (PM) {
                            if (newtime[0] === '12') {
                                hour = newtime[0]
                            } else {
                                hour = 12 + parseInt(newtime[0], 10)
                            }
                        } else {
                            if (newtime[0] === '12') {
                                newtime[0] = '00'
                                hour = newtime[0]
                            } else {
                                hour = newtime[0]
                            }
                        }

                        var newTime = hour + ':' + minNew;

                        var date = new Date();
                        var month = date.getMonth() + 1;
                        var day = date.getDate();

                        if (month <= 9) {
                            month = '0' + month;
                            if (day <= 9) {
                                day = '0' + day;
                            }
                        } else if (day <= 9) {
                            day = '0' + day;
                        }

                        var dt = date.getFullYear() + '-' + month + '-' + day;
                        const getTimeZone = ele.ipDetails.timezone

                        if (getTimeZone === '') {
                            const result0 = await LocalProfile.find({});
                            result0.forEach(ele2 => {
                                ele2.mac.forEach(ele0 => {
                                    if (ele0 === ele.mac) {
                                        const result1 = await User.findOne({ id: ele2.userId })
                                        console.log(result1.email, ele.mac)
                                    }
                                })
                            })
                        }

                        let a = ele1.scheduled.split(' ');
                        const timeSplit = newTime.split(':');
                        var getTime = timeSplit[0];
                        const getHoure1 = getTime.length === 1 ? `0${getTime}` : getTime;

                        const timeZoneTime = moment.tz(`${dt} ${getHoure1}:${timeSplit[1]}`, getTimeZone); // convert timezone
                        console.log(timeZoneTime, 'timeZone')

                        const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
                        const d = timeZoneTimeHu[1].split(':');
                        a[1] = d[1];
                        a[2] = d[0];
                        let c = a.join(' ');
                        console.log(c, 'updated Schedule')

                        const resultSch = await Schedule.findOneAndUpdate({ uid: ele1.uid }, { scheduled: c }, { new: true }).exec()
                    })
                }
            })
            res.json({ msg: 'success' })
        } catch (err) {
            next(err)
        }
    })

module.exports = router;