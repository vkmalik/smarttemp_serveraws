'use strict';
const express = require('express');
const router = express.Router();
const { Geo, GeoHistory } = require('../models');

router.route('/')

.post(async(req, res, next) => {
    const { mac, geoFenceId } = req.body || {}
    try {
        const geo = new Geo(req.body);
        geo.save();
        setTimeout(async function() {
            const result = await Geo.findOne({ mac, geoFenceId }).exec()
            res.json(result)
        }, 1000);

    } catch (err) {
        next(err)
    }
})

.put(async(req, res, next) => {
    const { body } = req || {}
    const { mac, geoFenceId } = req.body || {}
    try{
       const result = await Geo.findOneAndUpdate({ mac, geoFenceId },  body , { new : true }).exec()
       res.json(result)
    }catch(err){
       next(err)
    }
})

router.route('/delete')

.put(async(req, res, next) => {
    const { mac, geoFenceId } = req.body || {}
    try{
       const result = await Geo.findOneAndRemove({ mac, geoFenceId }).exec()
       res.json(result)
    }catch(err){
       next(err)
    }
})


router.route('/:id')

.get(async(req, res, next) => {
    const { id } = req.params || {}
    try {
        const result = await Geo.find({ mac : id }).exec()
        res.json(result)
    } catch (err) {
        next(err)
    }
})

router.route('/android/:id')

.get(async(req, res, next) => {
    const { id } = req.params || {}
    try {
        const result = await Geo.find({ mac : id }).exec()
        res.json({msg : result})
    } catch (err) {
        next(err)
    }
})

router.route('/history')

.post(async(req, res, next) => {
    try{
       const date = new Date();
       Object.assign( req.body,{ "time_stamp" : date })
       const geoHistory = new GeoHistory(req.body);
       geoHistory.save();
       res.json({msg: "success"})
    }catch(err){
       next(err)
    }
})


router.route('/history/:id')

.get(async(req, res, next) => {
    const { id } = req.params || {}
    try {
        const result = await GeoHistory.find({ mac : id }).exec()
        res.json(result)
    } catch (err) {
        next(err)
    }
})

router.route('/history/android/:id')

.get(async(req, res, next) => {
    const { id } = req.params || {}
    try {
        const result = await GeoHistory.find({ mac : id }).exec()
        res.json({msg : result})
    } catch (err) {
        next(err)
    }
})

module.exports = router;
