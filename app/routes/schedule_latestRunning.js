'use strict';
const express = require('express');
const router = express.Router();
const { Schedule, UserProfiles, DeviceParam } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const { scheduleUpdateServ } = require('../lib');
const moment = require('moment-timezone');
const request = require('request-promise-native');

// mob or web or device Insert Value

router.post('/', async(req, res, next) => {
    var { mac, userId, time, days } = req.body;
    const uid = uuid(15)
    try {
        const result = await LocalProfile.findOne({ userId, mac });
        if (!result) {
            const error = new Error('Please add the device to your account');
            error.status = 400;
            next(error);
        }

        var PM = time.match('PM') ? true : false
        var newtime = time.split(':')
        var minNew = newtime[1].split(' ')[0];
        var hour;
        if (PM) {
          if (newtime[0] === '12') {
                hour = newtime[0]
            } else {
                hour = 12 + parseInt(newtime[0], 10)
            }
        } else {
            if (newtime[0] === '12') {
                newtime[0] = '00'
                hour = newtime[0]
            } else {
                hour = newtime[0]
            }
        }

        var newTime = hour + ':' + minNew;

        var date = new Date();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        if (month <= 9) {
            month = '0' + month;
            if (day <= 9) {
                day = '0' + day;
            }
        } else if (day <= 9) {
            day = '0' + day;
        }
        var dt = date.getFullYear() + '-' + month + '-' + day;
        const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
        const getTimeZone = getDeviceResult.ipDetails.timezone
        let a = req.body.scheduled.split(' ');
        const timeSplit = newTime.split(':');
        var MorningTime = timeSplit[0];
        var getTime = timeSplit[0];
        const getHoure1 = getTime.length === 1 ? `0${getTime}` : getTime;
//	if (timeSplit[1] <= 9) {
  //          timeSplit[1] = '0' + timeSplit[1];
    //    }
        const timeZoneTime = moment.tz(`${dt} ${getHoure1}:${timeSplit[1]}`, getTimeZone); // convert timezone
        const min = timeZoneTime.format().split(':');
        const getAddHour = timeZoneTime.format().split(':')[2].split('');

        var scheduleDays = days;

        if (`${getHoure1}${timeSplit[1]}` - `${getAddHour[3]}${getAddHour[4]}${min[min.length - 1]}` < 0) {
            // for updating schedule days if it falls on the previous day as per the UTC time and date
            days = days.map(ele => {
                ele = ele + 1 === 7 ? 0 : ele + 1;
                console.log(ele, 'ele')
                return ele;
            })

            scheduleDays = scheduleDays.map(ele1 => {
                ele1 = ele1 - 1 === -1 ? 6 : ele1 - 1;
                console.log(ele1, 'ele1')
                return ele1;
            })
        }


        const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
        const d = timeZoneTimeHu[1].split(':');
        const scheduledDays = days;
        a[1] = d[1];
        a[2] = d[0];
        a[5] = scheduleDays.join();
        let c = a.join(' ');
        Object.assign(req.body, { scheduled: c, uid, scheduleTime: `${MorningTime}${timeSplit[1]}`, time, scheduledDays });

        if (req.body.temp_cool === 'Off') {
            req.body.temp_cool = 'off'
        }
        if (req.body.temp_heat === 'Off') {
            req.body.temp_heat = 'off'
        }
        const schedule = new Schedule(req.body);
        schedule.save();
        scheduleUpdateServ(req.body.scheduled, uid);
        return res.json({ success: true, message: "Added successfully", uid });
    } catch (error) {
        next(error);
    }
});

//  get Value


router.get('/:mac', async(req, res, next) => {
    const { mac } = req.params || {};
    try {
        const result = await Schedule.find({ mac, status: 'pending' }).sort({ scheduleTime: 1 });
        res.json(result);
    } catch (error) {
        next(error);
    }
});


router.get('/android/:mac', async(req, res, next) => {
    const { mac } = req.params || {};
    try {
        const result = await Schedule.find({ mac, status: 'pending' }).sort({ scheduleTime: 1 });
        res.json({ schedule: result });
    } catch (error) {
        next(error);
    }
});

// global schedule

router.put('/bulk', async(req, res, next) => {
    const { mac } = req.body || {};
    try {
        if (!mac) {
            return res.json({ success: false, message: 'Mac Id Must' })
        }
        await Schedule.updateMany({ mac }, req.body);
        return res.json({ success: true, message: "Updated successfully" });
    } catch (error) {
        next(error);
    }
});

//  Find And Update The Last values

router.put('/update', async(req, res, next) => {
    // console.log('update')
    var { uid, time, days } = req.body || {};
    try {
console.log(days, 'days');
        const result = await Schedule.findOne({ uid });
        if (!result) {
            return res.json({ success: false, message: "uid is mandatory" });
        }
        const { fan_speed, temp_cool, temp_heat, enable_disable, scheduled, overRide, overRideTime, name } = result;
        if (req.body.temp_cool === 'Off') {
            req.body.temp_cool = 'off'
        }
        if (req.body.temp_heat === 'Off') {
            req.body.temp_heat = 'off'
        }

        var PM = time.match('PM') ? true : false
        var newtime = time.split(':')
        var minNew = newtime[1].split(' ')[0];
        var hour;
        if (PM) {
           if (newtime[0] === '12') {
                hour = newtime[0]
            } else {
                hour = 12 + parseInt(newtime[0], 10)
            }
        } else {
            if (newtime[0] === '12') {
                newtime[0] = '00'
                hour = newtime[0]
            } else {
                hour = newtime[0]
            }
        }

        var newTime = hour + ':' + minNew;

        var date = new Date();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        if (month <= 9) {
            month = '0' + month;
            if (day <= 9) {
                day = '0' + day;
            }
        } else if (day <= 9) {
            day = '0' + day;
        }
        var dt = date.getFullYear() + '-' + month + '-' + day;

        const { mac } = result || {};
        const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
        const getTimeZone = getDeviceResult.ipDetails.timezone
        let a = req.body.scheduled.split(' ');
        const timeSplit = newTime.split(':');
        var MorningTime = timeSplit[0];
        var getTime = timeSplit[0];
        const getHoure1 = getTime.length === 1 ? `0${getTime}` : getTime;
//	if (timeSplit[1] <= 9) {
  //          timeSplit[1] = '0' + timeSplit[1];
    //    }
        const timeZoneTime = moment.tz(`${dt} ${getHoure1}:${timeSplit[1]}`, getTimeZone); // convert timezone
        const min = timeZoneTime.format().split(':');
        const getAddHour = timeZoneTime.format().split(':')[2].split('');
        var scheduleDays = days;

        if (`${getHoure1}${timeSplit[1]}` - `${getAddHour[3]}${getAddHour[4]}${min[min.length - 1]}` < 0) {
            // for updating schedule days if it falls on the previous day as per the UTC time and date
            days = days.map(ele => {
                ele = ele + 1 === 7 ? 0 : ele + 1;
                // console.log(ele, 'ele')
                return ele;
            })

            scheduleDays = scheduleDays.map(ele1 => {
                ele1 = ele1 - 1 === -1 ? 6 : ele1 - 1;
                // console.log(ele1, 'ele1')
                return ele1;
            })
        }


        const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
        const d = timeZoneTimeHu[1].split(':');
        const scheduledDays = days;
        a[1] = d[1];
        a[2] = d[0];
        a[5] = scheduleDays.join();
        let c = a.join(' ');
        Object.assign(req.body, { scheduled: c, uid, scheduleTime: `${MorningTime}${timeSplit[1]}`, time, scheduledDays, timezone: getTimeZone });
        const getOverData = Object.assign({ OverRideRunning: true, fan_speed, temp_cool, temp_heat, enable_disable, scheduled, overRide, overRideTime, name, scheduleRunning: false }, req.body)
        const rs = await Schedule.findOneAndUpdate({ uid }, getOverData, { new: true })
        scheduleUpdateServ(rs.scheduled, uid);
        res.json({ success: true, message: "Updated successfully" });

    } catch (error) {
        next(error);
    }
});

//  Find And delete

 router.delete('/delete', async(req, res, next) => {
     const { uid, days } = req.body;
//console.log(uid,'uid');
//console.log(typeof days,'days');
     try {
         const result = await Schedule.findOne({ uid });

         if (!result) {
             return res.json({ success: false, message: "uid is mandatory" });
         } else {
             if (days === 'All') {
                 await Schedule.findOneAndRemove({ uid });
             } else {
                 var daysArray = result.days;

               var a = daysArray.filter(ele => ele !== parseInt(days));
//console.log(a,'a');
                 var result1 = await Schedule.findOneAndUpdate({ uid }, { days: a }, { new: true });

                 await request.put({ url: 'http://13.238.117.231:3000/api/schedule/update', json: true, body: result1 });
                 // await request.put({ url: 'http://localhost:4000/api/schedule/update', json: true, body: result1 });
             }
         }

         res.json({ message: "Deleted successfully" });
     } catch (error) {
         next(error);
     }
 });


router.delete('/:mac', async(req, res, next) => {
    const { mac } = req.params || {};
    try {
        const result = await Schedule.remove({ mac });
        res.json(result);
    } catch (error) {
        next(error);
    }
});

 router.delete('/selected/uid', async(req, res, next) => {
console.log(req.body,'body');
     const { list } = req.body;
console.log(list, 'list');
     try {
         var counter = 0;
         list.forEach(async(ele) => {
             counter++;
             const result = await Schedule.findOne({ uid: ele.uid });

            if (!result) {
                 return res.json({ success: false, message: "uid is mandatory" });
             } else {
                 if (ele.days === 'All') {
                     await Schedule.findOneAndRemove({ uid: ele.uid });
                 } else {
                     var daysArray = result.days;
console.log(ele.days, 'days');

                     var a = daysArray.filter(ele1 => !ele.days.includes(ele1));

                     const result1 = await Schedule.findOneAndUpdate({ uid: ele.uid }, { days: a }, { new: true });
                     await request.put({ url: 'http://13.238.117.231:3000/api/schedule/update', json: true, body: result1 });
//                     // await request.put({ url: 'http://localhost:4000/api/schedule/update', json: true, body: result1 });

                 }
             }
         });

         if (counter === list.length) {
             res.json({ message: "Deleted successfully" });
         }

//         // const result = await Schedule.remove({ uid: { $in: uid } });
//         // res.json(result);
     } catch (error) {
         next(error);
     }
 });

//router.delete('/selected/uid', async (req, res, next) => {
//    const { uid } = req.body;
//    try {
//        const result = await Schedule.remove({ uid: { $in: uid } });
//        res.json(result);
//    } catch (error) {
//        next(error);
//    }
// });
 
// router.delete('/delete', async (req, res, next) => {
//    const { uid } = req.body;
//    try {
//        const result = await Schedule.findOneAndRemove({ uid });
//        if (!result) {
//            return res.json({ success: false, message: "uid is mandatory" });
//        }
//        res.json({ message: "Deleted successfully" });
//    } catch (error) {
//        next(error);
//    }
// });

router.delete('/selected/uid/:id', async(req, res, next) => {
    const { id } = req.params;
    try {
        const result = await Schedule.remove({ uid: { $in: id } });
        res.json(result);
    } catch (error) {
        next(error);
    }
});

// schedule update from tcp

router.put('/tcp', async(req, res, next) => {
const { mac, temp_heat,temp_cool,fan_speed } = req.body || {};
try {
if (!mac) {
return res.json({ success: false, message: 'Mac Id Must' })
}

console.log(mac, temp_heat,temp_cool,fan_speed, 'latest');

const result = await Schedule.findOne({ mac, scheduleRunning: true });
console.log(result.temp_heat, result.temp_cool, result.fan_speed, 'result');
if(parseFloat(result.temp_heat) === parseFloat(temp_heat) && parseFloat(result.fan_speed) === parseFloat(fan_speed) && parseFloat(result.temp_cool) === parseFloat(temp_cool)){
console.log('same values');
}else{
console.log('values changed');
await Schedule.findOneAndUpdate({ mac, scheduleRunning: true }, { scheduleRunning: false });
return res.json({ success: true, message: "Updated successfully" });
}
} catch (error) {
next(error);
}
});

module.exports = router;

