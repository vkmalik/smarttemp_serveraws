'use strict';
const net = require('net');
const PORT = 4443;
const ADDRESS = '0.0.0.0';
const { DeviceParam, DeviceParamHistory } = require('../models');
const uuid = require('uuid-pure').newId;
const clients = [];
const deviceIpList = new Map();



let server = net.createServer(onClientConnected);
server.listen(PORT, ADDRESS, () =>{
    console.log(`Server started at: ${ADDRESS}:${PORT}`);
});


function onClientConnected(socket) {
	console.log("req found")
    socket.name = `${socket.remoteAddress}:${socket.remotePort}`;
    deviceIpList.set(socket.name, socket)
    clients.push(socket, 'device data');
    socket.on('data', (data) => {
        var m = data.toString().replace(/[\n\r]*$/, '');
        const ObjData = JSON.parse(m);
        Object.assign(ObjData, { socketIpForMob: socket.name });
        const { mac } = ObjData;

        
        DeviceParam.findOneAndUpdate({ mac }, ObjData)
        .then(data => {
            const result = JSON.stringify(data)
            socket.write(result)
        })
        .catch(error =>{
            console.log(error)
        })
    });

    socket.on('end', () => {
        deviceIpList.delete(socket.name)
        console.log(`${socket.name} disconnected.`);
    });
}


module.exports = app => {
    app.post('/api/device-tcp', async (req, res) => {
        try {
            const { socketIpForMob } = req.body;
            const client = deviceIpList.get(socketIpForMob);
            if (client === undefined) {
                return;
            }
            const resData = JSON.stringify(req.body)
            client.write(`${resData}\n`);
            res.send('ok');
        } catch (error) {
            console.log(error)
        }
    })
}


