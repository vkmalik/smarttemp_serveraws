'use strict';

const express = require('express');
const router = express.Router({ mergeParams: true });
const path = require('path');
const { UserProfiles } = require('../../../models');
const { LocalProfile } = UserProfiles;
const multer = require('multer')

const storage = multer.diskStorage({
    destination: './uploads/profile-pictures',
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileName = `${file.fieldname}-${Date.now()}${ext}`;
        cb(null, fileName);
    }
});
const upload = multer({ storage: storage });


/**
 * Update Profile Pic  User
 */

router.route('/')

    .put(upload.single('photo'), async(req, res, next) => {
        const fields = Object.keys(req.body);
        const updatedData = {};
        fields.forEach(field => updatedData[field] = req.body[field]);

        if (req.file) {
            const { filename, path } = req.file || {};
            const [basePath, ...imagePath] = path.split('/');
            const profilePic = imagePath.join('/');
            updatedData['profilePic'] = `/${profilePic}`;
        }

        const { userId } = req.params;

        try {
            const result = await LocalProfile.findOneAndUpdate({ userId }, updatedData, { new: true });
            
            if (!result) {
                const error = new Error('User Not Exist');
                error.status = 400;
                return next(error);
            }

            res.json(result);
        } catch (error) {
            next(error);
        }
    })


module.exports = router;