'use strict';
const express = require('express');
const router = express.Router();
const { Schedule, UserProfiles, DeviceParam } = require('../models');
const { LocalProfile } = UserProfiles;
const uuid = require('uuid-pure').newId;
const _ = require('underscore');
const { scheduleUpdateServ } = require('../lib');
const moment = require('moment-timezone');


// mob or web or device Insert Value

router.post('/', async (req, res, next) => {
    const { mac, userId, time } = req.body;
    const uid = uuid(15)
    try {
        const result = await LocalProfile.findOne({ userId, mac });
        if (!result) {
            const error = new Error('Please add the device to your account');
            error.status = 400;
            next(error);
        }
        const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
      const getTimeZone = getDeviceResult.ipDetails.timezone || 'Australia/Melbourne' 
      console.log(getTimeZone)
//const getTimeZone = 'Australia/Melbourne';
        let a = req.body.scheduled.split(' ');
         let num = Number(a[2]);
         const getTimeMOn = time.split(' ');
         const timeSplit = getTimeMOn[0].split(':');
         if(getTimeMOn[1] === 'AM'){
             timeSplit[0] = timeSplit[0] == 12 ? '00' : timeSplit[0];
         }
         var getTime = getTimeMOn[1] === 'AM' ? timeSplit[0] : + timeSplit[0] + 12;
             getTime = getTimeZone === 'Australia/Melbourne' ? getTime + 1 : getTime;        
            // getTime = getTime == 25 ? '13' : getTime;
         const getHoure1 = getTime.length === 1 ? `0${getTime}` :  getTime;
         const getHoure = getHoure1 === 24 ? '12' : getHoure1;
console.log(getHoure, timeSplit[1])
         const timeZoneTime = moment.tz(`2013-11-18 ${ getHoure }:${timeSplit[1]}`, getTimeZone);
console.log(timeZoneTime)
         const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
         const d = timeZoneTimeHu[1].split(':');
         a[1] = d[1];
         a[2] = d[0];
         let c = a.join(' ');
        Object.assign(req.body, { scheduled: c, uid, scheduleTime: `${getHoure}${timeSplit[1]}`, time });
        scheduleUpdateServ(req.body.scheduled, uid);
        if(req.body.temp_cool === 'Off'){
              req.body.temp_cool = 'off'  
            }
            if(req.body.temp_heat === 'Off'){
              req.body.temp_heat = 'off'  
            }
        const schedule = new Schedule(req.body);
        schedule.save();
        return res.json({success: true, message: "Added successfully", uid });
    } catch (error) {
        next(error);
    }
});

//  get Value


router.get('/:mac', async (req, res, next) => {
    const { mac } = req.params || {};
    try {
        const result = await Schedule.find({ mac, status: 'pending' }).sort({time: 1});
        res.json(result);
    } catch (error) {
        next(error);
    }
});

// global schedule

router.put('/bulk', async (req, res, next) => {
    const { mac } = req.body || {};
    try {
        if(!mac){
            return res.json({ success: false, message: 'Mac Id Must'})
        }
const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
         const getTimeZone = getDeviceResult.ipDetails.timezone || 'Australia/Melbourne'
      console.log(getTimeZone)
//const getTimeZone = 'Australia/Melbourne';
        let a = req.body.scheduled.split(' ');
         let num = Number(a[2]);
         const getTimeMOn = time.split(' ');
         const timeSplit = getTimeMOn[0].split(':');
         if(getTimeMOn[1] === 'AM'){
             timeSplit[0] = timeSplit[0] == 12 ? '00' : timeSplit[0];
         }
         var getTime = getTimeMOn[1] === 'AM' ? timeSplit[0] : + timeSplit[0] + 12;
             getTime = getTimeZone === 'Australia/Melbourne' ? getTime + 1 : getTime;
            // getTime = getTime == 25 ? '13' : getTime;
         const getHoure1 = getTime.length === 1 ? `0${getTime}` :  getTime;
         const getHoure = getHoure1 === 24 ? '12' : getHoure1;
console.log(getHoure, timeSplit[1])
         const timeZoneTime = moment.tz(`2013-11-18 ${ getHoure }:${timeSplit[1]}`, getTimeZone);
console.log(timeZoneTime)
         const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
         const d = timeZoneTimeHu[1].split(':');
         a[1] = d[1];
         a[2] = d[0];
         let c = a.join(' ');
 Object.assign(req.body, { scheduled: c, uid, scheduleTime: `${getHoure}${timeSplit[1]}`, time });
         await Schedule.updateMany({ mac }, req.body);
        return res.json({ success: true, message: "Updated successfully" });
    } catch (error) {
        next(error);
    }
});

//  Find And Update The Last values

router.put('/update', async (req, res, next) => {
    const {uid, time} = req.body || {};
    try {
        const result = await Schedule.findOne({ uid });
        if(!result){
            return res.json({ success: false, message: "uid is mandatory" });
        }
            const  { fan_speed, temp_cool, temp_heat, enable_disable, scheduled, overRide, overRideTime, name } = result;
            if(req.body.temp_cool === 'Off'){
              req.body.temp_cool = 'off'  
            }
            if(req.body.temp_heat === 'Off'){
              req.body.temp_heat = 'off'  
            }
           // let a = req.body.scheduled.split(' ');
         //let num = Number(a[2])
         //a[2] =  num + 1;
         //req.body.scheduled  = a.join(' ');
const { mac } = result || {};
const getDeviceResult = await DeviceParam.findOne({ mac }).lean();
         const getTimeZone = getDeviceResult.ipDetails.timezone || 'Australia/Melbourne'
      console.log(getTimeZone)
//const getTimeZone = 'Australia/Melbourne';
        let a = req.body.scheduled.split(' ');
         let num = Number(a[2]);
         const getTimeMOn = time.split(' ');
         const timeSplit = getTimeMOn[0].split(':');
         if(getTimeMOn[1] === 'AM'){
             timeSplit[0] = timeSplit[0] == 12 ? '00' : timeSplit[0];
         }
         var getTime = getTimeMOn[1] === 'AM' ? timeSplit[0] : + timeSplit[0] + 12;
             getTime = getTimeZone === 'Australia/Melbourne' ? getTime + 1 : getTime;
            // getTime = getTime == 25 ? '13' : getTime;
         const getHoure1 = getTime.length === 1 ? `0${getTime}` :  getTime;
         const getHoure = getHoure1 === 24 ? '12' : getHoure1;
console.log(getHoure, timeSplit[1])
         const timeZoneTime = moment.tz(`2013-11-18 ${ getHoure }:${timeSplit[1]}`, getTimeZone);
console.log(timeZoneTime)
         const timeZoneTimeHu = timeZoneTime.utc().format().split('T');
         const d = timeZoneTimeHu[1].split(':');
         a[1] = d[1];
         a[2] = d[0];
         let c = a.join(' ');
 Object.assign(req.body, { scheduled: c, uid, scheduleTime: `${getHoure}${timeSplit[1]}`, time });
            const getOverData = Object.assign({ OverRideRunning: true, fan_speed, temp_cool, temp_heat, enable_disable, scheduled, overRide, overRideTime, name }, req.body)
            const rs = await Schedule.findOneAndUpdate({ uid }, getOverData, { new: true })
            scheduleUpdateServ(rs.scheduled, uid);
        res.json({success: true,  message: "Updated successfully" });

    } catch (error) {
        next(error);
    }
});

//  Find And delete

router.delete('/delete', async (req, res, next) => {
    const { uid } = req.body;
    try {
        const result = await Schedule.findOneAndRemove({ uid });
         if(!result){
            return res.json({ success: false, message: "uid is mandatory" });
        }
        res.json({ message: "Deleted successfully" });
    } catch (error) {
        next(error);
    }
});


router.delete('/:mac', async (req, res, next) => {
    const { mac } = req.params || {};
    try {
        const result = await Schedule.remove({ mac });
        res.json(result);
    } catch (error) {
        next(error);
    }
});

router.delete('/selected/uid', async (req, res, next) => {
    const { uid } = req.body;
    try {
        const result = await Schedule.remove({ uid: {$in: uid } });
        res.json(result);
    } catch (error) {
        next(error);
    }
});

module.exports = router;
