'use strict';
const express = require('express');
const router = express.Router();
const { Blog } = require('../models');
const { AuthServ } = require('../lib');

router.route('/')
    .get(async(req, res, next) => {
        const query = {};

        try {
            const results = await Blog.find(query).exec();
            res.json(results);
        } catch (error) {
            next(error);
        }

    })
    .post(AuthServ.authorize(), async(req, res, next) => {
        const {
            body
        } = req;

        try {
            const blog = new Blog(body);
            const result = await blog.save();
            res.json(result);
        } catch (error) {
            next(error);
        }

    });

router.route('/:id')

    .put(async(req, res, next) => {
        const {
            body
        } = req;
        const { id } = req.params;
        const updateOptions = { rawResult: true };

        try {
            const blog = new Blog(body);
            const result = await Blog.findOneAndUpdate({ id }, body, updateOptions).exec();
            res.json(result);
        } catch (error) {
            next(error);
        }
    })


module.exports = router;