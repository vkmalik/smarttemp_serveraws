'use strict';
const {
	errorHandlingMiddleware,
	AuthServ
 } = require('../lib');
const usersRouter = require('./users');
const authRouter = require('./auth');
const blogsRouter = require('./blogs');
const contactRouter = require('./contact-us');
const deviceRouter = require('./device-update');
const addDeviceRouter = require('./device-list');
const scheduleRouter = require('./schedule');
const deviceRulesRouter = require('./device-rules');


module.exports = app => {
    app.use('/api/auth', authRouter);
    app.use('/api/users', usersRouter);
    app.use('/api/blogs', blogsRouter);
    app.use('/api/contact', contactRouter);
    app.use('/SMT-400', deviceRouter);
    app.use('/api/add-device', addDeviceRouter);
    app.use('/api/schedule', scheduleRouter);
    app.use('/api/device/rules', deviceRulesRouter);
    require('./mob-app-tcp')(app);
    app.use(errorHandlingMiddleware);
};
