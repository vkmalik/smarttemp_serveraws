'use strict';
require('dotenv').config();
const config = require('config');
const express = require('express');
const PORT = process.env.NODE_PORT || 3000;
const IP = config.get('IP');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const morgan = require('morgan');

const server = require('http').createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket) {
    socket.on('connect', function(data) {
        socket.emit('receive', ' dfuvghjdkfgdkfghjkhgshnskghkfufsknkhfsh');
    });

    socket.on('delete', function(data) {
        socket.emit('scheduleDelete', data);
    });
});


app.use(morgan('tiny'));


app.use(express.static('uploads'))

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use('/smt', express.static('public'))
app.use('', express.static('public1'))
require('./app/routes')(app);
require('./app/routes/schedule.js').io;
const onServerStart = () => {
    const ENVIROINMENT = process.env.NODE_ENV || 'development';
    const message = `Server Listening On Port ${PORT}, ENVIROINMENT=${ENVIROINMENT}`;
    console.log(message);
};

//app.listen(PORT, IP, onServerStart);
server.listen(PORT, IP, onServerStart);